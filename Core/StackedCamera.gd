extends Camera2D


const max_zoom_level = 0.7
const min_zoom_level = 9.0
const zoom_increment = 0.08


var start_pos

var _current_zoom_level = 1
var _drag = false

var pan_speed := 800.0

func _ready():
	_current_zoom_level = zoom.x
	start_pos = position
	CameraStack.push_camera(self)

func _process(delta):
	if Input.is_key_pressed(KEY_W):
		position.y -= pan_speed * delta
	if Input.is_key_pressed(KEY_A):
		position.x -= pan_speed * delta
	if Input.is_key_pressed(KEY_S):
		position.y += pan_speed * delta
	if Input.is_key_pressed(KEY_D):
		position.x += pan_speed * delta
	
	if CameraStack.trauma > 0:
		offset = CameraStack.offset
		#rotation = CameraStack.rotation
	else:
		offset = Vector2.ZERO
		rotation = 0.0

func _input(event):
	if event.is_action_pressed("cam_drag"):
		_drag = true
	elif event.is_action_released("cam_drag"):
		_drag = false
	elif event.is_action("cam_zoom_in"):
		_update_zoom(-zoom_increment, get_local_mouse_position())
	elif event.is_action("cam_zoom_out"):
		_update_zoom(zoom_increment, get_local_mouse_position())
	elif event is InputEventMouseMotion && _drag:
		position -= event.relative * _current_zoom_level

func _update_zoom(incr, zoom_anchor):
	var old_zoom = _current_zoom_level
	_current_zoom_level += incr
	if _current_zoom_level < max_zoom_level:
		_current_zoom_level = max_zoom_level
	elif _current_zoom_level > min_zoom_level:
		_current_zoom_level = min_zoom_level
	if old_zoom == _current_zoom_level:
		return
	
	set_zoom(Vector2(_current_zoom_level, _current_zoom_level))
