extends Node


const part_max_health := 90.0
const num_parts := 7
const ship_max_health := num_parts * part_max_health

signal damage(message)

export var global_fac := 1.5

export var rand_min := 0.5
export var rand_max := 2.7

export var damage_min := 0.52
export var damage_max := 1.25

export var progress_min := 0.3
export var progress_max := 2.4

export var threshold := 1.5

export var memory := 3

export(String, FILE) var message_file

export var victory: PackedScene
export var failure: PackedScene

var messages: Array2D

enum PART {
	oxygen = 0, generator = 1, cryo = 2, electrical = 3, 
	navigation = 4, shield = 5, mainframe = 6
}

var damages = {}

var last_damages := Array()

var progress := 0.0

var last_message := ""

var over := false
var won := false

var last_part = -1


func _ready():
	reset()
	damages[PART.cryo] = 50.0
	#begin()
	var data = CSVFile.new()
	data.load(message_file)
	messages = data.get_array2d()

func reset():
	last_message = ""
	last_damages = []
	progress = 0.0
	for i in range(0, num_parts):
			damages[i] = part_max_health
	over = false
	won = false
	last_part = -1

func begin():
	randomize()
	$Timer.start()
	print("Begin director")

func tick():
	var damage_fac = _evaluate_damage()
	var rand_fac = lerp(rand_min, rand_max, randf())
	var progress_fac = lerp(progress_min, progress_max, progress / 100.0)
	var fail = global_fac * damage_fac * progress_fac * rand_fac
	print(fail)
	if fail > threshold:
		create_event()

func create_event():
	var part = _pick_part()
	var severity = randi() % 10 + 1
	var damage = _sev_dmg(severity)
	var old_hp = damages[part]
	damages[part] -= damage
	last_part = part
	if old_hp > 30 and damages[part] <= 0:
		damages[part] = 1
	last_message = _get_message(part, damage)
	Soundboard.play("Damage")
	emit_signal("damage", last_message)
	_evaluate_damage()
	CameraStack.trauma += min(damage / 8.0, 3.5)
	if damages[part] <= 0:
		# TODO effects probably
		won = false
		over = true
		Soundboard.stop_all()
		Soundboard.stop("Music")
		Soundboard.play("GameOver")
		Soundboard.muted = true
		$Timer.stop()

func end():
	if won:
		Fader.fade_to(victory)
	else:
		Fader.fade_to(failure)
	$Timer.stop()

func _process(delta):
	if progress >= 100.0 and not over:
		won = true
		over = true
		Soundboard.stop_all()
		Soundboard.play("GameWin")
		Soundboard.muted = true
		$Timer.stop()

func _pick_part():
	var pick = randi() % num_parts
	while last_damages.has(pick):
		pick = randi() % num_parts
	
	last_damages.push_back(pick)
	if last_damages.size() > memory:
		last_damages.pop_front()
	
	return pick

func _sev_dmg(sev):
	if sev < 5:
		return 10
	elif sev < 8:
		return 20
	elif sev < 10:
		return 30
	else:
		return 40

func _get_message(part, dmg):
	for row in messages.get_rows():
		if row[0] == str(part) and row[2] == str(dmg):
			return row[3] + "\nRepair " + row[1]
	return "Unknown error"

func _evaluate_damage():
	var total_health = 0.0
	var red = false
	for health in damages:
		if damages[health] <= 30:
			red = true
		total_health += damages[health]
	if red:
		Soundboard.play_if_not("RedZone")
	else:
		Soundboard.stop("RedZone")
	var percent_health = total_health / ship_max_health
	return lerp(damage_min, damage_max, percent_health)
