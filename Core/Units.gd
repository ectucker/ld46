extends Node


var all_units: Array
var all_agents: Array

var selected_units: Array


func register_unit(unit):
	all_units.append(unit)
	all_agents.append(unit.agent)

func select_unit(unit):
	selected_units.append(unit)

func deselect_unit(unit):
	selected_units.erase(unit)

func clear_selection():
	for unit in selected_units:
		unit.deselect()
	selected_units.clear()

func reset():
	all_units = []
	all_agents = []
	selected_units = []

func _input(event):
	if event.is_action_pressed("clear_selection"):
		clear_selection()
