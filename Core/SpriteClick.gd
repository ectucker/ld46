class_name SpriteClick
extends KinematicBody2D


signal on_pressed(button_index)
signal on_released(button_index)

var rect


func _ready():
	var shape = shape_owner_get_shape(0, 0)
	var radius = shape.radius
	var pos = -Vector2(radius, radius)
	var size = Vector2(2 * radius, 2 * radius)
	rect = Rect2(pos, size)

func _unhandled_input(event):
	if event is InputEventMouseButton and event.pressed and not event.is_echo():
		if get_rect().has_point(to_local(get_global_mouse_position())):
			emit_signal("on_pressed", event.button_index)
			get_tree().set_input_as_handled()

	if event is InputEventMouseButton and not event.pressed and not event.is_echo():
		if get_rect().has_point(to_local(get_global_mouse_position())):
			emit_signal("on_released", event.button_index)
			get_tree().set_input_as_handled()

func get_rect():
	return rect
