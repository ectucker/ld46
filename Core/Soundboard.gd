extends Node


var muted := false


func play_if_not(sound: String):
	if not muted and not get_node(sound).playing:
		get_node(sound).play()

func play(sound: String):
	if not muted:
		get_node(sound).play()

func stop(sound: String):
	get_node(sound).stop()

func stop_all():
	for child in get_children():
		if child.name != "Music":
			child.stop()
