extends Node


var cameras := Array()

var trauma := 0.0

var trauma_decay := 2.0
var trauma_scale := 30
var trauma_scale_rot := 2.0

var offset := Vector2.ZERO
var rotation := 0.0

func push_camera(camera):
	cameras.push_back(camera)

func pop_camera():
	return cameras.pop_back()

func peek_camera():
	return cameras.back()

func clear():
	cameras = Array()

func _input(event):
	if event.is_action_pressed("fullscreen") and not OS.get_name() == "HTML5":
		OS.window_fullscreen = !OS.window_fullscreen

func _process(delta):
	trauma -= trauma_decay * delta
	if trauma < 0:
		trauma = 0.0
	offset.x = (randf() - 0.5) * trauma * trauma * trauma_scale
	offset.y = (randf() - 0.5) * trauma * trauma * trauma_scale
	rotation = (randf() - 0.5) * trauma * trauma * trauma_scale_rot
