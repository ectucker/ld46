extends Node


var state := 0
var begun := false
var finished := false


signal advance(next_message)


func _process(delta):
	#print(state)
	if begun:
		match state:
			0:
				emit_signal("advance", "Left Click to select Units\nDrag a box around them")
				state += 1
			1:
				if Units.selected_units.size() > 0:
					state += 1
			2:
				emit_signal("advance", "Right Click to move bots\nBots navigate where you click")
				state += 1
			3:
				if Units.selected_units.size() > 0 and Units.selected_units.front().is_moving:
					state += 1
			4:
				emit_signal("advance", "Drag Middle Click to pan\nYou can also use WASD for this")
				state += 1
			5:
				if CameraStack.peek_camera().position != CameraStack.peek_camera().start_pos:
					state += 1
			6:
				Director.last_part = 2
				emit_signal("advance", "Send bots to open the cryo bay\nPlace five on the orange plate")
				state += 1
			7:
				pass
			8:
				Director.last_part = 2
				emit_signal("advance", "Move to the blue tile to repair\nMore bots will repair faster")
				state += 1
			9:
				if Director.damages[Director.PART.cryo] >= 60.0:
					state += 1
			10:
				Director.last_part = -1
				emit_signal("advance", "Place bots on the thrusters\nExtra bots will increase speed")
				state += 1
			11:
				if Director.progress > 0.0:
					state += 1
			12:
				emit_signal("advance", "Good luck on your voyage!\nKeep the ship alive for me")
				Director.begin()
				finished = true
				state += 1
	
	if Director.progress > 0.0 and not finished:
		Director.last_part = -1
		state = 12

func cryo_open():
	if state == 7:
		state = 8
