extends Sprite


var drag_start: Vector2
var drag_end: Vector2

func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			drag_start = get_global_mouse_position()
	
	if event is InputEventMouseMotion:
		if Input.is_mouse_button_pressed(BUTTON_LEFT) and drag_start != null:
			drag_end = get_global_mouse_position()
			visible = true
			scale.x = (drag_end.x - drag_start.x) / 10
			scale.y = (drag_end.y - drag_start.y) / 10
			position = drag_start
	
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and not event.pressed and visible:
			Units.clear_selection()
			var rect = _build_rect(drag_start, drag_end)
			for unit in Units.all_units:
				var unit_box = unit.get_rect()
				unit_box = Rect2(unit.to_global(unit_box.position), unit_box.size)
				
				if rect.intersects(unit_box):
					unit.select()
					Units.select_unit(unit)
			visible = false

func _build_rect(start, end):
	var size = end - start
	size.x = abs(size.x)
	size.y = abs(size.y)
	
	var pos = Vector2.ZERO
	pos.x = min(start.x, end.x)
	pos.y = min(start.y, end.y)
	
	return Rect2(pos, size)
