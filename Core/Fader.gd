extends CanvasLayer


var next: PackedScene
var loaded: Node

func fade_to(next_scene: PackedScene):
	next = next_scene
	$AnimationPlayer.play("FadeOut")

func transition():
	if not Tutor.begun:
		Tutor.begun = true
	Soundboard.muted = false
	Soundboard.stop_all()
	get_tree().change_scene_to(next)
	$AnimationPlayer.play("FadeIn")
