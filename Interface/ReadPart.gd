extends Panel


export var part := 0

onready var green := $Bars/Green
onready var yellow := $Bars/Yellow
onready var red := $Bars/Red


func _process(delta):
	var health = Director.damages[part]
	green.set_value(max(health - 60, 0))
	yellow.set_value(min(max(health - 30, 0), 30))
	red.set_value(min(health, 30))
