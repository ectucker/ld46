extends Button


func _ready():
	connect("pressed", self, "exit")
	if OS.get_name() == "HTML5":
		queue_free()

func exit():
	Soundboard.play("ButtonPress")
	get_tree().quit()
