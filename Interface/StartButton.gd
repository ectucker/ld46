extends Button

var done := false

export var next_scene: PackedScene


func _ready():
	connect("pressed", self, "begin")

func begin():
	if not done:
		Soundboard.play("ButtonPress")
		Soundboard.play_if_not("Music")
		Fader.fade_to(next_scene)
		if Tutor.finished:
			Units.reset()
			CameraStack.clear()
			Director.reset()
			Director.begin()
		done = true
