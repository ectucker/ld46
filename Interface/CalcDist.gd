extends Label


const million = 1e6
const billion = 1e9
const trillion = 1e12

const max_miles = 25 * trillion
const max_au = 265000
const max_lightyears = 4.2

func _ready():
	var percent = Director.progress / 100.0
	text = """You failed to reach Proxima Centauri B\nBut you did travel:\n%.1f Light Years\n%s AU\n%s Miles""" \
		% [percent * max_lightyears, humanize_num(percent * max_au), humanize_num(percent * max_miles)]

func humanize_num(num: float):
	if num > trillion:
		return "%.1f Trillion" % (num / trillion)
	if num > billion:
		return "%.1f Billion" % (num / billion)
	if num > million:
		return "%.1f Million" % (num / million)
	else:
		return "%.f" % num
