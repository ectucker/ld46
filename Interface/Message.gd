extends Label


export(Array, Texture) var parts

onready var part_symbol = get_parent().get_node("Symbol")

func _ready():
	Director.connect("damage", self, "new_message")
	Tutor.connect("advance", self, "new_message")

func new_message(message):
	text = message
	if Director.last_part != -1:
		print("set texture")
		part_symbol.texture = parts[Director.last_part]
	else:
		part_symbol.texture = null
