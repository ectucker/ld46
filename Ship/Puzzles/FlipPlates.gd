extends Node


signal workers_changed(num_workers)

export var correct_up: Texture
export var correct_down: Texture
export var wrong_up: Texture
export var wrong_down: Texture

export var left_sprite_path: NodePath
export var right_sprite_path: NodePath

var left_sprite
var right_sprite

var flipped = true

var workers_1 := 0
var workers_2 := 0

func _ready():
	left_sprite = get_node(left_sprite_path)
	right_sprite = get_node(right_sprite_path)
	update_plates()

func flip():
	flipped = not flipped
	if flipped:
		emit_signal("workers_changed", workers_2)
	else:
		emit_signal("workers_changed", workers_1)
	update_plates()

func set_workers_1(num):
	workers_1 = num
	if not flipped:
		emit_signal("workers_changed", workers_1)

func set_workers_2(num):
	workers_2 = num
	if flipped:
		emit_signal("workers_changed", workers_2)

func update_plates():
	if flipped:
		left_sprite.plate_up = wrong_up
		left_sprite.plate_down = wrong_down
		right_sprite.plate_up = correct_up
		right_sprite.plate_down = correct_down
	else:
		right_sprite.plate_up = wrong_up
		right_sprite.plate_down = wrong_down
		left_sprite.plate_up = correct_up
		left_sprite.plate_down = correct_down
	left_sprite.set_num(left_sprite.num)
	right_sprite.set_num(right_sprite.num)
