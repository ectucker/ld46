extends Node


const nav = 4

export var wrong_scale := 1.0
export var work_speed := 0.2

export var correct_up: Texture
export var correct_down: Texture
export var wrong_up: Texture
export var wrong_down: Texture

export var left_sprite_path: NodePath
export var right_sprite_path: NodePath

var left_sprite
var right_sprite

var workers_left := 0
var workers_right := 0

var current_dir := 0


func _ready():
	left_sprite = get_node(left_sprite_path)
	right_sprite = get_node(right_sprite_path)
	update_plates()

func _process(delta):
	if Director.damages[nav] < 90.0 and current_dir == 0:
		current_dir = 1
		update_plates()
	
	var lean_left = workers_left - workers_right
	var lean_right = workers_right - workers_left
	match current_dir:
		1:
			if lean_right > 0:
				Director.damages[nav] += lean_right * work_speed * delta
			else:
				Director.damages[nav] += lean_right * work_speed * delta * wrong_scale
		-1:
			if lean_left > 0:
				Director.damages[nav] += lean_left * work_speed * delta
			else:
				Director.damages[nav] += lean_left * work_speed * delta * wrong_scale
		0:
			if lean_left > 0:
				Director.damages[nav] -= lean_left * work_speed * delta * wrong_scale
				current_dir = 1
				update_plates()
			elif lean_right > 0:
				Director.damages[nav] -= lean_right * work_speed * delta * wrong_scale
				current_dir = -1
				update_plates()
	
	if Director.damages[nav] >= 90.0:
		current_dir = 0
	
	Director.damages[nav] = clamp(Director.damages[nav], 0.0, 90.0)

func update_plates():
	match current_dir:
		1:
			left_sprite.plate_up = wrong_up
			left_sprite.plate_down = wrong_down
			right_sprite.plate_up = correct_up
			right_sprite.plate_down = correct_down
		-1:
			right_sprite.plate_up = wrong_up
			right_sprite.plate_down = wrong_down
			left_sprite.plate_up = correct_up
			left_sprite.plate_down = correct_down
		0:
			left_sprite.plate_up = wrong_up
			left_sprite.plate_down = wrong_down
			right_sprite.plate_up = wrong_up
			right_sprite.plate_down = correct_down
	left_sprite.set_num(left_sprite.num)
	right_sprite.set_num(right_sprite.num)

func set_workers_left(num):
	workers_left = num

func set_workers_right(num):
	workers_right = num
