class_name Thruster
extends Node2D


const move_speed := 0.028

var workers := 0

onready var particles = get_parent().get_node("Particles")

func _process(delta):
	if Director.progress < 100.0 and not Director.over:
		Director.progress += min(workers, 15) * move_speed * delta

func set_workers(num):
	workers = num
	if workers > 0:
		if not particles.emitting:
			Soundboard.play("ThrusterOn")
		particles.emitting = true
	else:
		if particles.emitting:
			Soundboard.stop("ThrusterOn")
			Soundboard.play("ThrusterOff")
		particles.emitting = false


func num_bots(num_bots):
	pass # Replace with function body.


func num_workers():
	pass # Replace with function body.


func sum_workers():
	pass # Replace with function body.
