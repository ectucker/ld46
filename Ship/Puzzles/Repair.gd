class_name Repair
extends Node2D


export var work_speed := 0.2
export var part := 0

var workers := 0

onready var green := $Green
onready var yellow := $Yellow
onready var red := $Red


func _process(delta):
	if Director.damages[part] < 90.0:
		Director.damages[part] += workers * work_speed * delta
		Director.damages[part] = clamp(Director.damages[part], 0.0, 90.0)
		if Director.damages[part] == 90.0:
			Soundboard.play("RepairFinished")
	
	var current_hp = Director.damages[part]
	if current_hp > 60:
		green.visible = true
		yellow.visible = false
		red.visible = false
	elif current_hp > 30:
		green.visible = false
		yellow.visible = true
		red.visible = false
	else:
		green.visible = false
		yellow.visible = false
		red.visible = true

func set_workers(num):
	if num > workers:
		Soundboard.play("RepairProgress")
	workers = num


func num_bots(num_bots):
	pass # Replace with function body.


func _on_BluePlate1_weight_changed(num_bots):
	pass # Replace with function body.


func num_workers():
	pass # Replace with function body.
