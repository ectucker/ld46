class_name Thresholder
extends Node2D


signal activated
signal deactivated

export var threshold := 10

var current := 0


func set_num(num):
	if num >= threshold and not current >= threshold:
		emit_signal("activated")
	elif num < threshold and not current < threshold:
		emit_signal("deactivated")
	
	current = num

func connect_tutor():
	connect("activated", Tutor, "cryo_open")

func bot_num(num_bots):
	pass # Replace with function body.
