extends Sprite


export var plate_up: Texture
export var plate_down: Texture

export var down_threshold := 1

var num = 0

func _ready():
	texture = plate_up

func set_num(new_val):
	if new_val >= down_threshold:
		texture = plate_down
		if not num >= down_threshold:
			Soundboard.play("PlateDown")
	elif new_val < down_threshold:
		texture = plate_up
		if not num < down_threshold:
			Soundboard.play("PlateUp")
	num = new_val
