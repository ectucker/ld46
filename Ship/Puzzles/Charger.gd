extends BotTracker


var recharge_rate := 25.0


func _process(delta):
	for bot in bots:
		bot.charge += recharge_rate * delta
		bot.charge = max(bot.charge, 100.0)
