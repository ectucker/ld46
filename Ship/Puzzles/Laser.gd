class_name Laser
extends StaticBody2D


onready var start_pos := global_position


func activate():
	visible = true
	global_position = start_pos
	Soundboard.play("LaserOn")

func deactivate():
	visible = false
	global_position = Vector2(100000, 1000000)
	Soundboard.play("LaserOff")
