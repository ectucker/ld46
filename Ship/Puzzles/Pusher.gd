class_name Pusher
extends BotTracker


export var direction: Vector2
export var strength := 32000.0

var active = true


func _ready():
	connect("body_exited", self, "clear_force")

func _physics_process(delta):
	for bot in bots:
		if active:
			bot.external_acc = direction * strength
		else:
			bot.external_acc = Vector2.ZERO

func clear_force(body: Node):
	if body.is_in_group("Bots"):
		body.external_acc = Vector2.ZERO

func activate():
	Soundboard.play("Fan")
	active = true
	$Particles.emitting = true

func deactivate():
	active = false
	$Particles.emitting = false
