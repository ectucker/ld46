class_name Gauge
extends Node2D


export var min_angle := deg2rad(-140.0)
export var max_angle := deg2rad(40.0)

export var max_value := 20.0

var value := 0.0


func set_value(num):
	value = num
	rotation = lerp(min_angle, max_angle, value / max_value)
