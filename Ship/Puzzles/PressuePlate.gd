class_name BotTracker
extends Area2D


signal weight_changed(num_bots)

var bots := Array()
var sprite: Sprite


func _ready():
	connect("body_entered", self, "_body_entered")
	connect("body_exited", self, "_body_exited")
	sprite = get_node("Sprite")

func _body_entered(body: Node):
	if body.is_in_group("Bots"):
		bots.append(body)
		emit_signal("weight_changed", bots.size())
		if sprite != null:
			sprite.set_num(bots.size())

func _body_exited(body: Node):
	if body.is_in_group("Bots"):
		bots.erase(body)
		emit_signal("weight_changed", bots.size())
		if sprite != null:
			sprite.set_num(bots.size())
