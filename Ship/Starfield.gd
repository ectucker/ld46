tool
extends ParallaxLayer


export var stars_in_sector := 30

var startTileX := -4
var startTileY := -4
var numTilesX := 8
var numTilesY := 8

func _draw():
	for x in range(startTileX, startTileX + numTilesX):
		for y in range(startTileY, startTileY + numTilesY):
			seed(hash(Vector2(x, y) * motion_scale))
			for i in range(stars_in_sector):
				draw_circle(Vector2(256 * x + randf() * 256, 256 * y + randf() * 256), 1, Color(1, 1, 1))
