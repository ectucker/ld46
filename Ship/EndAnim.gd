extends AnimationPlayer


var end := false


func _process(delta):
	if not end:
		if Director.won:
			play("Victory")
			end = true
		elif Director.over:
			play("Death")
			end = true

func end():
	Director.end()
