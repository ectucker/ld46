extends ParallaxBackground


func _process(delta):
	for layer in get_children():
		layer.motion_offset.x = Director.progress * -10000.0 * layer.motion_scale.x
