class_name Robot
extends SpriteClick


const max_accel := 10000.0
const max_speed := 90000.0

const max_accel_low := 8000.0
const max_speed_low := 60000.0

const drag := 0.16

const path_offset := 20.0
const prediction_time := 0.3
const deceleration_radius := 100.0
const arrival_tolerance := 10.0

const bound_radius := 22.0
const proximity_radius := 30.0

const charge_loss := 10.0

var selected := false

onready var navigation := get_node("../../Navigation2D")

var _accel := GSAITargetAcceleration.new()
var _has_path := false

var is_moving := false

onready var agent := GSAIKinematicBody2DAgent.new(self)

onready var path := GSAIPath.new(_empty_path(), true)
onready var follow := GSAIFollowPath.new(agent, path, 0, 0)

onready var proximity := GSAIRadiusProximity.new(agent, [], proximity_radius)
onready var seperation := GSAISeparation.new(agent, proximity)

onready var priority := GSAIPriority.new(agent, 0.0001)
onready var blend := GSAIBlend.new(agent)

var external_acc := Vector2.ZERO

var sprite

var time_since_last_path := 100.0

var target: Vector2

export var charge := 100.0

func _ready():
	Units.register_unit(self)
	
	connect("on_pressed", self, "_on_pressed")
	
	follow.path_offset = path_offset
	follow.prediction_time = prediction_time
	follow.deceleration_radius = deceleration_radius
	follow.arrival_tolerance = arrival_tolerance
	
	proximity.radius = proximity_radius
	proximity.agents = Units.all_agents

	agent.linear_acceleration_max = max_accel
	agent.linear_speed_max = max_speed
	agent.linear_drag_percentage = drag
	agent.bounding_radius = bound_radius
	
	seperation.decay_coefficient = 200000.0
	
	blend.add(follow, 0.5)
	blend.add(seperation, 8.5)
	
	var i = randi() % 15 + 1
	var path = "res://Characters/Bots/R%d.tscn" % i
	var bot = load(path)
	sprite = bot.instance()
	self.add_child(sprite)
	sprite.position = Vector2.ZERO
	sprite.scale = Vector2(0.1, 0.1)
	if randi() % 2 == 0:
		sprite.scale.x = -0.1

func _process(delta):
	if _has_path:
		blend.calculate_steering(_accel)
		time_since_last_path += delta
		if time_since_last_path > 2:
			repath(target)
	else:
		#seperation.calculate_steering(_accel)
		pass
	
	sprite.rotation = _accel.linear.x * -0.00003
	if abs(_accel.linear.x) < 3000:
		sprite.rotation = 0.0
	else:
		if _accel.linear.x > 0:
			sprite.scale.x = -0.1
		else:
			sprite.scale.x = 0.1
	
	_accel.linear += Vector3(external_acc.x, external_acc.y, 0)
	#agent._apply_steering(_accel, delta)
	
	if is_moving and target.distance_squared_to(position) < arrival_tolerance * arrival_tolerance:
		is_moving = false
	
	if is_moving:
		#charge -= charge_loss * delta
		#charge = max(charge, 0.0)
		#agent.linear_speed_max = lerp(max_speed_low, max_speed, charge / 100.0)
		#agent.linear_acceleration_max = lerp(max_accel_low, max_accel, charge / 100.0)
		pass

func _physics_process(delta):
	agent._apply_steering(_accel, delta)

func _unhandled_input(event):
	if selected and event is InputEventMouseButton:
		if event.button_index == BUTTON_RIGHT and event.pressed:
			repath(get_global_mouse_position())
		Soundboard.play_if_not("BotMove")
	elif selected and event is InputEventMouseMotion:
		if Input.is_mouse_button_pressed(BUTTON_RIGHT) \
			and time_since_last_path > 0.1:
			repath(get_global_mouse_position())

func repath(goal):
	var points = navigation.get_simple_path(global_position, goal)
	var positions := PoolVector3Array()
	for p in points:
		positions.append(Vector3(p.x, p.y, 0))
	path.create_path(positions)
	_has_path = true
	is_moving = true
	time_since_last_path = 0.0
	var end = path.get_end_point()
	target = Vector2(end.x, end.y)

func _on_pressed(button_index):
	pass

func _empty_path():
	return [Vector3(global_position.x, global_position.y, 0),
		Vector3(global_position.x, global_position.y, 0)]

func _remove_dupes(points):
	var cleared = PoolVector2Array()
	for i in range(0, points.size()):
		if i == 0:
			cleared.append(points[i])
		elif not points[i] == points[i - 1]:
			cleared.append(points[i])
	return cleared

func select():
	selected = true
	get_node("Select").play()
	modulate = Color.lightblue
	sprite.get_material().set_shader_param("outline_width", 20.0)
	sprite.get_material().set_shader_param("outline_color", Color("293E5E"))

func deselect():
	selected = false
	modulate = Color.white
	sprite.get_material().set_shader_param("outline_width", 0.0)
