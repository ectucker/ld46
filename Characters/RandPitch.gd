extends AudioStreamPlayer


export var min_scale := 0.8
export var max_scale := 1.2


func _ready():
	pitch_scale = rand_range(min_scale, max_scale)
